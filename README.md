# Best/Rough
This project aims to improve quality, consistency and overall development speed.

## Structure

* best
	* docs (what you're reading now, generated with StyleDocco)
	* framework
		* best
			* stylesheets
				* best (the main scss)
			* templates
				* project (boilerplate for new projects)

## Roadmap
* Create a ruby gem so it's easier to install

## How to use
Install it. Reference it, configure it. Apply it.
Remember to check the config.rb and adjust to your local system paths.