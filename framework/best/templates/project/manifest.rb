# Make sure you list all the project template files here in the manifest.

description "Best/Rough Starter Template"

discover: all

#stylesheet 'sass/style.scss', :media => 'screen, projection'
#stylesheet 'sass/_config.scss'
#stylesheet 'sass/_base.scss'
#stylesheet 'sass/_layout.scss'
#stylesheet 'sass/_module.scss'
#stylesheet 'sass/_theme.scss'
#stylesheet 'ie.scss',     :media => 'screen, projection', :condition => "lt IE 9"

#image 'gui.png'
#javascript 'script.js'

#html 'welcome.html.haml', :erb => true
#file 'README'

help %Q{
This is a message that users will see if they type

  compass help my_extension

You can use it to help them learn how to use your extension.
}

welcome_message %Q{
This is a message that users will see after they install this pattern.
Use this to tell users what to do next.
	YES WHAT TO DO. Hm, oskar oskar.
}
