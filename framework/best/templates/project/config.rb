# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"

environment = :development
# environment = :production

output_style = (environment == :production) ? :compressed : :expanded

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = true
color_output = false 

# require "/Library/Ruby/Gems/1.8/gems/susy-1.0.rc.1/lib/susy.rb"
require 'susy'

add_import_path "/Users/stefansinnwell/Documents/Scripts/compass/lib"
add_import_path "/Users/oskar/Documents/Work/assets/best/framework/best/stylesheets"
add_import_path "/Users/oskar/Documents/Work/assets/rem/stylesheets"



# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
